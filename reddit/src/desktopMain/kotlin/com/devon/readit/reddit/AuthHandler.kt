package com.devon.readit.reddit

import com.sun.net.httpserver.HttpExchange
import com.sun.net.httpserver.HttpHandler
import com.sun.net.httpserver.HttpServer
import io.ktor.client.request.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

import java.awt.Desktop
import java.net.InetSocketAddress
import java.net.ServerSocket
import java.net.Socket
import java.net.URI

actual class AuthHandler {

    private var server : HttpServer? = null

    private val scope = CoroutineScope(Dispatchers.IO)

    actual fun startAuth(){
        val desktop = Desktop.getDesktop()

        val state = "RANDOM_STRING"

        desktop.browse(URI.create("https://www.reddit.com/api/v1/authorize?client_id=6kNsLyImJtxIjA&response_type=code&" +
                "state=$state&redirect_uri=http://localhost:7667/authorize&duration=temporary&scope=identity"))

        if (server == null){
            server = HttpServer.create(InetSocketAddress("localhost",7667), 0).apply {
                createContext("/authorize"
                ) {  exchange ->
                    val query = exchange.requestURI.query
                        .split("&")
                        .map {
                            val (first,second,_) = it.split("=")
                            first to second
                        }
                        .toMap()
                    if ( query["state"] == state){
                        println("State Correct")
                        scope.launch {
                            query["code"]?.let { flow.emit(it) }
                        }
                    } else {
                        println("State Incorrect")
                    }
                    exchange.sendResponseHeaders(200,0)
                    exchange.close()
                    server?.stop(0)
                }
                start()
            }
        }
    }

    private val flow = MutableSharedFlow<String>()
    actual val codes: Flow<String> = flow
}