package com.devon.readit.reddit

import kotlinx.coroutines.flow.Flow

expect class AuthHandler() {
    fun startAuth()
    val codes: Flow<String>
}