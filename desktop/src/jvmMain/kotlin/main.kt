import com.devon.readit.commonCompose.App
import androidx.compose.desktop.Window

fun main() = Window {
    App()
}