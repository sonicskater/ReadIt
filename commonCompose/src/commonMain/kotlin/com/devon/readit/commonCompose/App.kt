package com.devon.readit.commonCompose
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import com.devon.readit.reddit.AuthHandler
import androidx.compose.material.Text
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers

val handler = AuthHandler()

@Composable
fun App() {

    val code by handler.codes.collectAsState("")

    var text by remember { mutableStateOf("Hello, World!") }

    MaterialTheme {

        Column(Modifier.fillMaxSize(), Arrangement.spacedBy(5.dp)) {
            Button(
                modifier = Modifier.align(Alignment.CenterHorizontally),
                onClick = {
                    text = "Hello, ${getPlatformName()}"
                }
            )
            {
                Text(text)
            }
            Button(
                modifier = Modifier.align(Alignment.CenterHorizontally),
                onClick = {
                    handler.startAuth()
                }
            ){
                Text("Login")
            }
            Text(code)
        }
    }
}

@Composable
fun RedditLogin() {
    MaterialTheme {

    }
}
